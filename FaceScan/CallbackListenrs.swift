//
//  CallbackListenrs.swift
//  VisionExample
//
//  Created by Techsaga Corp on 13/12/22.
//  Copyright © 2022 Google Inc. All rights reserved.
//

import Foundation

//protocol CallbackListenrsDelegate {
//    func onCompleteRequestFrame(type: String, percentage: Int, fps: Int, isFaceDetected: Bool)
//
//    func onCompleteRequestScanFinished(raw_intensity: String, ppg_time: String, average_fps: Int, finalData: String)
//
//    func onCompleteRequestCancelScan(cancelError: String)
//
//}
//
//class CallbackListenrs {
//    var delegate: CallbackListenrsDelegate?
//
//    func onFrame(type: String, percentage: Int, fps: Int, isFaceDetected: Bool) {
//        delegate?.onCompleteRequestFrame(type: type, percentage: percentage, fps: fps, isFaceDetected: isFaceDetected)
//    }
//
//    func onScanFinished(raw_intensity: String, ppg_time: String, average_fps: Int, finalData: String) {
//        delegate?.onCompleteRequestScanFinished(raw_intensity: raw_intensity, ppg_time: ppg_time, average_fps: average_fps, finalData: finalData)
//    }
//
//    func onCancelScan(cancelError: String) {
//        delegate?.onCompleteRequestCancelScan(cancelError: cancelError)
//    }
//}
public protocol CallbackListenrs: AnyObject {
    
    func onFrame(type: String, percentage: Int, fps: Int, isFaceDetected: Bool)
    
    func onScanFinished(raw_intensity: String, ppg_time: String, average_fps: Int, finalData: String)
    
    func onCancelScan(cancelError: String)
    
}
